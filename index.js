const express = require("express");
const mongoose = require("mongoose");
const apiv1 = require("./api/v1");
const app = express();
const port = 3000;

async function main() {
  await mongoose.connect('mongodb://localhost:27017/test');

  app.use("/api/v1", apiv1);

  app.get("/", (req, res) => {
    res.send("Hello World!");
  });

  app.listen(port, () => {
    console.log("Application running on port", port);
  });
}

main().catch(err => console.log(err));
