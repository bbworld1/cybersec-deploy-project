const mongoose = require("mongoose");

const MomSchema = new mongoose.Schema({
  name: String,
  mass: Number
});

const Mom = new mongoose.model("Mom", MomSchema);

module.exports = {
  Mom, MomSchema
};
