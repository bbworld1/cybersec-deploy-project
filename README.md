# cybersec-deploy-project

This project is a bit difficult to deploy. Can you deploy it?

Here's how you need to run it:

Step 1: Clone this project.
Step 2: Install and deploy MongoDB.
Step 3: `npm install`.
Step 4: `npm start`.

You can either deploy using Docker (you'll have to write the Dockerfile yourself),
or do it manually. The application has to have all API routes working
(`/api/v1/mom` and `/api/v1/mom/make`), and it has to start on boot.
