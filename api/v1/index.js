const express = require("express");
const { Mom, MomSchema } = require("../../models");
const router = express.Router();

router.get("/", (req, res) => {
  res.json({ version: 1 });
});

router.get("/mom", async (req, res) => {
  res.send(await Mom.find({}));
});

router.post("/mom/make", async (req, res) => {
  const mom = new Mom({ name: "Your Mom", mass: 94 });
  await mom.save();
  res.json({ "result": "success" });
});

module.exports = router;
